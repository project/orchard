<div class="<?php print "block block-$block->module" ?>" id="<?php  print "block-$block->module-$block->delta"; ?>">
  <h2><?php print $block->subject ?></h2>
  <div class="content"><?php print $block->content ?>
   <?php if ($block->module == "block"):?>
       <?php if (user_access('administer blocks')) :?>
      <br /><center><a href='/admin/block/edit/<?php print $block->delta;?>'>[edit]</a></center>
       <?php endif; ?>
   <?php endif; ?>
  </div>
</div>
