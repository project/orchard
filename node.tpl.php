<?php
$type = (node_invoke($node->type, 'node_name'));
//print $type gives the content type label rather than the module name
// better than print $node->type hack
?>
<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0): ?>
    <div class="title"><a href=" <?php print $node_url ?> " title=" <?php print $title ?> "> <?php print $title ?> </a></div>
  <?php endif; ?>
  <div class="info"><?php print $date ?></div>
  <?php print $picture ?>
  <div class="content">
    <?php print $content ?>
    <br style="clear:both;"/>
  </div>
  <?php if ($links): ?>
     <?php if ($terms): ?><div class="terms">tag: <?php print $terms ?> </div><?php endif; ?>
     <div class="links"> <?php print $links ?> </div>
  <?php endif; ?>
</div>
