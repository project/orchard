<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
	<head>
	<title><?php print $head_title ?></title>
	<?php //to correct the unsightly Flash of Unstyled Content. http://www.bluerobot.com/web/css/fouc.asp ?>
	<script type="text/javascript"></script>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if lt IE 7]>
    <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie.css";</style>
    <![endif]-->
	</head>
<body>
<div id="globalwrap">
	<div id="header">
  <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
	<?php if ($site_name) { ?><h1 id='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
  <?php if ($logo) { ?><div id="site-logo"><a href="<?php print $base_path ?>" title="<?php print $site_name ?>"><img src="<?php print($logo) ?>" alt="<?php print $site_name ?>" /></a></div><?php } ?>
	<?php if ($site_slogan) { ?><div id='site-slogan'><?php print $site_slogan ?></div><?php } ?>
	</div>
  <div id="menu">
		<div id="primary">
		 <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
		</div>
		<div id="secondary">
		  <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
		</div>		
	</div>
	<div id="main">
		<div id="customheader"><?php print($header) ?></div>
    <br style="clear:both;" />
		<?php if ($mission): ?><p id="mission"><?php print $mission ?></p><?php endif; ?>
		<div id="wrapper">
			<div id="content">
				<?php if ($breadcrumb != ""): ?><div id="breadcrumb"><?php print $breadcrumb ?></div><?php endif; ?>
				<h1 class="title"><?php print $title ?></h1>
				<div class="tabs"><?php print $tabs ?></div>
				<?php if ($help != ""): ?><p id="help"><?php print $help ?></p><?php endif; ?>
				<?php if ($messages != ""): ?><div id="message"><?php print $messages ?></div><?php endif; ?>
				<?php print($content) ?>
			</div>
			<?php if (($sidebar_right != "") || ($sidebar_left != "")): ?>
			<div id="sidebar">
				<?php print $sidebar_right ?>
				<?php print $sidebar_left ?>
			</div>
			<?php endif; ?>
		</div>
		<br style="clear:both">
	</div>
		<div id="footer">
			<?php if ($footer_message) : ?>
			<?php print $footer_message;?>
			<?php endif; ?>
			<!--please leave these links. These are "The Orchard" template authors. Leaving these links you contribute their work-->
			<p>Powered by <a href="http://www.drupal.org">Drupal</a> and The Orchard theme by <a title="OSWD design work" href="http://www.oswd.org/user/profile/id/12139">furado</a> and <a title="drupal port and redesign" href="http://blog.psicomante.net">Psicomante</a>.</p>
		</div>
</div>

<?php print $closure;?>
</body>
</html>
