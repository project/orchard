<?php

function _phptemplate_variables($hook, $vars = array()) {
  switch ($hook) {
    case 'page':
      if ((arg(0) == 'admin')) {
        $vars['template_file'] = 'page-admin';
      }
      break;
  }
	static $count;
  $count = is_array($count) ? $count : array();
  $count[$hook] = is_int($count[$hook]) ? $count[$hook] : 1;
  $vars['zebra'] = ($count[$hook] % 2) ?'odd' : 'even';
  $vars['seqid'] = $count[$hook]++;
  return $vars;
}

function orchard_regions() {
  return array(
    'right' => t('right sidebar'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer'),
    'floater' => t('floater'),
    'content_header' => t('content header')
  );
}
?>